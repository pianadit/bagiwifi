<html>
  <head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
	<title>Halaman Login</title>
  </head>
  
<body id="LoginForm">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/css_login.css');?>"/>
	<div class="container">
		<div class="login-form">
		<div class="main-div">
			<div class="panel">
		   <h2>Aplikasi Admin Sebaran Lokasi WiFi</h2>
		   <p>Silahkan masukan Username dan Password untuk login</p>
		   </div>
		   <?php
				$info = $this->session->flashdata("info");
				if(!empty($info)){
					echo $info;
				}
		   ?>
			<form id="Login" name="form_login" action="<?php echo site_url("Login/proses_login")?>" method="post">

				<div class="form-group">


					<input type="text" class="form-control" name="username" placeholder="Username">

				</div>

				<div class="form-group">

					<input type="password" class="form-control" name="password" placeholder="Password">

				</div>
				<button type="submit" class="btn btn-primary">Login</button>

			</form>
		</div>
		</div>
	</div>
</body>
</html>
