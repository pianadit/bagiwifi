<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('user/user') ?>">
            <i class="fas fa-home fa-fw"></i>
            <span>Home</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('user/home/preview_data') ?>">
            <i class="fas fa fa-folder"></i>
            <span>Data Diri</span></a>
    </li> 
       

    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('user/home/data_ranking') ?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Hasil Rekomendasi</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('login/logout') ?>">
            <i class="fas fa-fw fa-cog"></i>
            <span>Keluar</span></a>
    </li>
</ul>