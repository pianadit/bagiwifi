<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('user/head')?>
</head>

<body id='page-top'>
	<?php $this->load->view('user/navbar')?>

	<div id="wrapper">
		<?php $this->load->view('user/sidebar')?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<div class="content-header" align="center">
					<h3>Formulir Lengkapi Biodata</h3>
				</div>

				<!-- DKonten -->
				<div class="card mb-3">
					<div class="card-body">
						<form action="<?php echo site_url('user/user/lengkapi_biodata')?>" method="post">
							<?php $info=$this->session->flashdata('error');
								if (!empty($info))
								{
									echo $info;
								}
							?>
						      
						<div class="header">
							<h3 align="center" style="background: silver">Data Siswa</h3>						  
						</div>

						<div class="row">
							<div class="form-group col-sm-3">
							    <label >Kode Pendaftaran</label>
							      <input type="text" class="form-control"  name="kode_regist" value="<?php echo $registrasi->kode_regist;?>" readonly>
							</div>	
							<div class="form-group col-sm-3">
							   <label>NISN</label>
								    <input type="text" class="form-control"  placeholder="NISN" name="nisn" pattern="[0-9]+" title="only letters" required >
							</div>
						</div>

						  <div class="form-group">
						    <label >Nama Lengkap</label>
						        <input type="text" class="form-control col-sm-6"  placeholder="Nama" name="nama" value ="<?php echo $registrasi->nama;?>" readonly>
						  </div>

							<div class="form-group">
							<label>Jenis Kelamin</label>
							<div class="form-check form-check-inline">
							  <input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="Laki-laki" checked>
							  <label class="form-check-label" for="inlineRadio2">Laki-laki</label>
							</div>
							<div class="form-check form-check-inline">
							  <input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio3" value="Perempuan">
							  <label class="form-check-label" for="inlineRadio3">Perempuan</label>
							</div>
							</div>						

						  <div class="row">
						    <div class="form-group col-sm-4">
						      <label >Tempat Lahir</label>
						      <input type="tet" class="form-control"  name="tempat" placeholder="Tempat Lahir" pattern="[A-Za-Z][A-Za-z' -]+" title="Isi dengan huruf" required>
						    </div>
						    <div class="form-group col-sm-2">
						      <label for="inputPassword4">Tanggal Lahir</label>
						      <input type="date" class="form-control" name="tgl_lahir"  placeholder="Tanggal Lahir" required>
						    </div>						    
						  </div>
							
						  <div class="form-group">
						    <label >Agama</label>
						      <select class="form-control col-sm-6" name="agama">
						      	<option value="" disabled="">Agama</option>
								  <option value="Islam">Islam</option>
								  <option value="Kristen">Kristen</option>
								  <option value="Katolik">Katolik</option>
								  <option value="Hindu">Hindu</option>
								  <option value="Buddha">Buddha</option>
							</select>
						    </div>
						  </div>

						<div class="header">
							<h3 align="center" style="background: silver">Jurusan yang Diinginkan</h3>
						</div>
						 <div class="form-group">
						    <label >Jurusan</label>
						      <select class="form-control col-sm-6" name="jurusan">
						      	<option value="">Pilih Jurusan</option>
						      	<?php foreach($jurusan as $data){?>
						      		<option value="<?= $data->id_jurusan?>"><?= $data->jurusan?></option>
								 <?php }?>
							</select>
						    </div>
						  </div>


						<!-- batas -->
						<div class="header">
							<h3 align="center" style="background: silver">Data Nilai</h3>
						</div>					
						<div class="form-group">
					    	<label for="inputPassword3" class="col-sm-3 control-label">Nilai Bahasa Inggris</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="inggris" required >
						    </div>
					  	</div>					
						<div class="form-group">
					    	<label for="inputPassword3" class="col-sm-3 control-label">Nilai Bahasa Indonesia</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="indonesia" required >
						    </div>
					  	</div>					
						<div class="form-group">
					    	<label for="inputPassword3" class="col-sm-3 control-label">Nilai Matematika</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="matematika" required >
						    </div>
					  	</div>					
						<div class="form-group">
					    	<label for="inputPassword3" class="col-sm-3 control-label">Nilai Rata-rata UN</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="rata-rata" required >
						    </div>
					  	</div>					
						<div class="form-group">
					    	<label for="inputPassword3" class="col-sm-3 control-label">Pendapatan</label>
						    <div class="col-sm-9">
						    	<select class="form-control" name="pendapatan" id="pendapatan">
						    		<option>-- Pilih Pendapatan --</option>>
						    		<?php foreach ($pendapatan as $k){?>
							      	<option value="<?= $k->nilai ?>"><?= $k->pendapatan ?></option>
							      <?php };?>
								</select>
						    </div>
					  	</div>				  

						
						<!-- Data orang Tua -->
						<div class="header">
							<h3 align="center" style="background: silver">Data Orang Tua</h3>					  
						</div>						    
							
						  <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Nama Ayah</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="nama_ayah" placeholder="Nama Ayah" pattern="[A-Za-z' -]+" title="Isi dengan huruf" required>
						    </div>
						  </div>
						  
						  <div class="form-group">
						    <label for="inputPassword3" class="col-sm-3 control-label">Nama Ibu</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="nama_ibu" placeholder="Nama Ibu" pattern="[A-Za-z' -]+" title="Isi dengan huruf" required>
						    </div>
						  </div>
						  
						  <div class="form-group">
						    <label for="inputPassword3" class="col-sm-3 control-label">Pendidikan Tertinggi Ayah</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="pendidikan_ayah" placeholder="Pendidikan Tertinggi Ayah" pattern="[A-Za-z' -]+" title="Isi dengan huruf" required>
						    </div>
						  </div>
						  
						  <div class="form-group">
						    <label for="inputPassword3" class="col-sm-3 control-label">Pendidikan Tertinggi Ibu</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="pendidikan_ibu"  placeholder="Pendidikan Tertinggi Ibu" pattern="[A-Za-z' -]+" title="Isi dengan huruf" required>
						    </div>
						  </div>  

						  							
						<input type="submit" class="submit" name="submit">
						</form>
												
						  </div>  
						</div>

					</div>
				</div> <!-- card mb-3 -->
			</div>

			<?php $this->load->view("user/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("user/scrolltop") ?>
	<?php $this->load->view("user/modal") ?>

	<?php $this->load->view("user/js") ?>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$('#dataTables').DataTable();
		});
	</script>

	<script type="text/javascript">
		function deleteConfirm(url){
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}
	</script>

	<!-- <script type="text/javascript">
		$('#pendapatan').change(function(){
			alert($(this).val());
		});
	</script> -->

</body>
</html>