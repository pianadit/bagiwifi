<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('user/head')?>
	<?php $data = $this->Model_login->lihat_hasil();
		if($data != null){
			$data_session = array(
	            'nama_jurusan'=>$data->jurusan,
	            'jurusan'=>$data->id_jurusan,
        	);
        }else{
        	redirect(site_url('user/home'));
        }
        $this->session->set_userdata($data_session);
	?>
</head>

<body id='page-top'>
	<?php $this->load->view('user/navbar')?>

	<div id="wrapper">
		<?php $this->load->view('user/sidebar')?>
		<div id="content-wrapper">
			<div class="container-fluid">

				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-body">
						<div class="card mb-3" style="border-color: black">
				          <div class="card-header" style="text-align: center; background-color: skyblue">
				            <b><h4>Tabel Perangkingan</h4></b>
							<small><h4><?= $_SESSION['nama_jurusan']?></h4></small>
				          </div>
				          <div class="card-body">
				            <div class="table-responsive">
				              	<table class="table table-bordered" id="dataTables" width="100%" cellspacing="0">
					                <thead>
					                  <tr>
					                    <th><i>Ranking</i></th>
					                    <th>Nama</th>
					                    <th>Nilai</th>
					                  </tr>
					                </thead>
					                <?php 
					                	@$a = $_SESSION['jurusan'];
							              if ($a == 1){
							                @$jurus = 'hasil_tkr';
							              }else if ($a == 2){
							                @$jurus = 'hasil_atph';
							              }else if ($a == 3){
							                @$jurus = 'hasil_tsm';
							              }else if ($a == 4){
							                @$jurus = 'hasil_rpl';
							              }else{
							                @$jurus = 'hasil_aphp';
							              }?>
							        <?php $data = $this->M_master_data->rank_siswa($jurus); ?>
					                <tbody>              
					                  <?php $no=1; foreach($data as $rank) :?>
					                    <tr>
					                        <td width="150">
					                          <?php echo $no++ ?>
					                        </td>
					                        <td>
					                          <?php echo $rank->nama ?>
					                        </td>
					                        <td>
					                          <?php echo $rank->nilai ?>
					                        </td>
					                      </tr>
					                   <?php endforeach?>
					                </tbody>
				              	</table>
				            </div>
				          </div>
				        </div>
					</div>
				</div>
			</div>

			<?php $this->load->view("user/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("user/modal") ?>

	<?php $this->load->view("user/js") ?>
	
</body>
<script type="text/javascript">
    $(document).ready(function(){
      $('#dataTables').DataTable();
    });
  </script>
</html>