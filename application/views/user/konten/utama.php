<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('user/head')?>
</head>

<body id='page-top'>
	<?php $this->load->view('user/navbar')?>

	<div id="wrapper">
		<?php $this->load->view('user/sidebar')?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<?php $this->load->view('user/breadcrumbs')?>

				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-body">

						<div class="jumbotron">
						  <h1>Selamat Datang <?php echo $this->session->userdata('nama');?>! </h1>
						  <p>Jika anda belum melengkapi biodata, silahkan melengkapinya dengan mengklik <b>Biodata</b> dibawah ini</p>
						  
						  <p><a class="btn btn-primary btn-lg" href="<?php echo site_url('user/home/tampil_biodata/'.$this->session->userdata('kode_regist'));?>" role="button">Biodata</a></p>
						  <hr>
						  <h3><p><div class="alert alert-success" role="alert">Akun anda <?php echo $this->session->userdata('status');?>!</div> </p></h3>						  
						</div>
						
					</div>
				</div>
			</div>

			<?php $this->load->view("user/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("user/js") ?>

</body>
</html>