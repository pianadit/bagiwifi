<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('user/head')?>
</head>

<body id='page-top'>
	<?php $this->load->view('user/navbar')?>

	<div id="wrapper">
		<?php $this->load->view('user/sidebar')?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<?php $this->load->view('user/breadcrumbs')?>

				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-body">

						<div class="table-responsive">
							<table class="table table-hover" id="dataTables" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>Kode Registrasi</th>
										<th>nisn</th>
										<th>nama</th>
										<th>Jenis Kelamin</th>
										<th>username</th>
										<th>password</th>
										<th>No. telepon</th>
										<th>level</th>
										<!-- <th>tempat lahir</th>
										<th>tanggal lahir</th>
										<th>agama</th>
										<th>nilai un</th>
										<th>nama ayah</th>
										<th>nama ibu</th>
										<th>pekrjaan ayah</th>
										<th>pekerjaan ibu</th> -->
									</tr>
								</thead>
				
								<tbody>
									<?php if( ! empty($registrasi)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
									 foreach ($registrasi as $a){ ?>
									<tr>
										<td width="150">
											<?php echo $a->kode_regist ?>
										</td>
										<td>
											<?php echo $a->nisn ?>
										</td>
										<td>
											<?php echo $a->nama ?>
										</td>
										<td>
											<?php echo $a->jenis_kelamin ?>
										</td>
										<td>
											<?php echo $a->username ?>
										</td><td>
											<?php echo $a->password ?>
										</td>
										<td>
											<?php echo $a->telepon ?>
										</td>
										<td>
											<?php echo $a->level ?>
										</td>
										
										
											<a href="<?php echo site_url('admin/as/edit/'.$a->a_id) ?>"
											 class="btn btn-small">
											 <i class="fas fa-edit"></i> Edit</a>
											<a onclick="deleteConfirm('<?php echo site_url('admin/as/delete/'.$a->a_id) ?>')"
											 href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td>
									</tr>

									<?php } //end foreach
									} else{ // Jika data siswa kosong
        								echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
									} ?>

								</tbody>

							</table>
							
						</div>
					</div>
				</div>
			</div>

			<?php $this->load->view("user/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("user/scrolltop") ?>
	<?php $this->load->view("user/modal") ?>

	<?php $this->load->view("user/js") ?>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$('#dataTables').DataTable();
		});
	</script>

	<script type="text/javascript">
		function deleteConfirm(url){
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}
	</script>

</body>
</html>