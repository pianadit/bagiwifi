<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="text/stylesheet" href='<?php echo base_url('assets/DataTables/dataTables.min.css')?>'></link>
	<script type="text/javascript" src="<?php echo base_url('assets/DataTables/dataTables.min.js')?>"></script>
</head>
<body>

<div class="container">
	<h1>Welcome to CodeIgniter!</h1>
	<div class="table-responsive">
	<table class="table table-bordered table-hover " id="table">
      <thead>
        <tr>      
          <th>ID</th>
          <th>Kode Barang</th>
          <th>Nama Barang</th>
          <th style="text-align:center">Action</th>
        </tr>
      </thead>
     <tbody>
			        <?php foreach ($data_admin as $b){ ?> 
				<tr>				
					<td><?php echo $b->id_admin; ?></td>
					<td><?php echo $b->username; ?></td>
					<td><?php echo $b->password; ?></td>
                                        <td align="center">
                                        <a href="#">
										<input type="button" value="Edit" class="tombol small gray"></a>
                                        <a href="#" onclick="return confirm('Anda yakin Ingin menghapus Data ?')">
                                        <input type="button" value="Hapus" class="tombol small gray"></a>
                                        </td>
				</tr>
                             <?php } ?>
			</tbody>>
    </table>
    		<!-- <div>
    			<label><H1>Kode Kriteria</H1></label>
            	<input type="text" name="id_kriteria" readonly="" value="<?php echo $kode ?>">
          	</div> -->
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
<script type="text/javascript">
	$(document).ready(function(){
		$('#table').DataTable();
	});
</script>
</html>