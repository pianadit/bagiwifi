<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('welcome') ?>">
            <i class="fas fa-home fa-fw"></i>
            <span>Home</span>
        </a>
    </li>

       
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('welcome/prosedur') ?>">
            <i class="fas fa-fw fa-info-circle""></i>
            <span>Prosedur Pendaftaran</span></a>
    </li>
    
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('welcome/tampil_regist')?>">
            <i class="fas fa-fw fa-user"></i>
            <span>Registrasi</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('login')?>">
            <i class="fas fa-fw fa-cog"></i>
            <span>Login</span></a>
    </li>
</ul>
