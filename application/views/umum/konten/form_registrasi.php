<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('umum/head')?>
</head>

<body id='page-top'>
	<?php $this->load->view('umum/navbar')?>

	<div id="wrapper">
		<?php $this->load->view('umum/sidebar')?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<div class="content-header" align="center">
					<h3>Formulir Pendaftaran</h3>
				</div>

				<!-- Konten -->
				<div class="card mb-3">
					<div class="card-body">

						<form action="<?php echo site_url('welcome/registrasi')?>" method="post">
							<?php $info=$this->session->flashdata('error');
								if (!empty($info))
								{
									echo $info;
								}
							?>
							<div class="form-group">
								<label for="name">Kode Registrasi</label>
								<input class="form-control" type="text" name="kode_registrasi" value="<?php echo $kode; ?>" readonly="readonly"/>								
							</div>

							<div class="form-group">
								<label>Nama Lengkap*</label>
								<input class="form-control" type="text" name="nama" placeholder="Nama Lengkap" required="text" pattern="[A-Za-Z][A-Za-z' -]+" title="Isi dengan huruf" required/>								
							</div>

							<div class="form-group">
								<label>Username*</label>
								<input class="form-control" type="text" name="username" required='text' placeholder="Username" required="text" />								
							</div>

							<div class="form-group">
								<label>Password*</label>
								<input class="form-control" type="password" name="password" placeholder="Password" required="text" />								
							</div>

							<div class="form-group">
								<label>No. Telepon/HP*</label>
								<input class="form-control" type="text" name="telepon" placeholder="No. Telepon/HP"  pattern="^\d{12}$" title="Isi dengan 12 no HP" required />								
							</div>

							<input type="submit" name="submit" value="Simpan">
							<a href="<?php echo site_url('admin/kriteria'); ?>"><input type="button" value="Batal"></a>						
						</form>

					</div>
				</div>
			</div>

			<?php $this->load->view("umum/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("user/modal") ?>

	<?php $this->load->view("umum/js") ?>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$('#dataTables').DataTable();
		});
	</script>

	<script type="text/javascript">
		function deleteConfirm(url){
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}
	</script>

</body>
</html>