<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('umum/head')?>
</head>

<body id='page-top'>
	<?php $this->load->view('umum/navbar')?>

	<div id="wrapper">
		<?php $this->load->view('umum/sidebar')?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-body">

						<h3 align="center">Prosedur Pendaftaran</h3>
<hr>
	<ol><h4>
        <li>
            <strong>Pendaftaran.</strong> Calon siswa baru melakukan pendaftaran pada website PPDB Online, melalui halaman
            <?php echo anchor('welcome/tampil_regist', 'registrasi'); ?>.
            Kemudian calon siswa baru melakukan login dengan username dan password yang telah didaftarkan sebelumnya untuk melengkapi Biodata.
        </li><br/>

        <li><strong>Mengisi biodata.</strong>Setelah login dengan akun yang sudah terdaftar lalu melengkapi biodata pada form yang yang telah disediakan </li><br/>

        <li>
            <strong>Tunggu Hasil.</strong> calon siswa menunggu hasil rekomendasi setelah melengkapi biodata.
        </li><br/>
  
        <li><strong>Pengumuman.</strong> Pengumuman hasil seleksi akan diumumkan pada waktu yang telah ditentukan.</li><br/>
        
    </h4>
</ol>
					</div>
				</div>
			</div>

			<?php $this->load->view("umum/footer") ?>
			
		</div>

	</div>

	<?php $this->load->view("umum/js") ?>

</body>
</html>