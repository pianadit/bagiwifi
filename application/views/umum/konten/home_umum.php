<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('umum/head')?>
</head>

<body id='page-top'>
	<?php $this->load->view('umum/navbar')?>

	<div id="wrapper">
		<?php $this->load->view('umum/sidebar')?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-body">

						<div class="jumbotron">
  <h1>Selamat Datang Calon Siswa Baru SMKN 1 Pusakanagara</h1>

  <p>Sebelum melakukan pendaftaran, sebaiknya anda memahami prosedur pendaftaran terlebih dahulu dihalaman 
  <b><?php echo anchor('welcome/prosedur','Prosedur Pendaftaran');?></b></p>
    
  <p>Jika anda sudah memahami prosedur pendaftaran, silahkan klik tombol <b>Registrasi</b> dibawah ini untuk melakukan <b>Pendaftaran!</b></p>
  
  <p><a class="btn btn-primary btn-lg" href="<?php echo site_url('welcome/tampil_regist');?>" role="button">Registrasi</a></p>
</div>
					</div>
				</div>
			</div>

			<?php $this->load->view("umum/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("user/modal") ?>

	<?php $this->load->view("umum/js") ?>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$('#dataTables').DataTable();
		});
	</script>

	<script type="text/javascript">
		function deleteConfirm(url){
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}
	</script>

</body>
</html>