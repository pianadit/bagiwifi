<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('admin') ?>">
            <i class="fas fa-home fa-fw"></i>
            <span>Home</span>
        </a>
    </li>
    
    
    

    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'products' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa fa-folder"></i>
            <span>Master Data</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/Kriteria') ?>">Kriteria</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/home/data_siswa') ?>">Data Pendaftar</a>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/home/data_sebaran') ?>">
            <i class="fas fa-fw fa-wifi"></i>
            <span>Data Sebaran</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('login/logout')?>">
            <i class="fas fa-fw fa-cog"></i>
            <span>Logout</span></a>
    </li>
</ul>