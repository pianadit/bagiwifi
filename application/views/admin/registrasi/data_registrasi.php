<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('admin/_partials/head')?>
</head>

<body id='page-top'>
  <?php $this->load->view('admin/_partials/navbar')?>

  <div id="wrapper">
    <?php $this->load->view('admin/_partials/sidebar')?>
    <div id="content-wrapper">
      <div class="container-fluid">
        <?php $this->load->view('admin/_partials/breadcrumbs')?>

        <!-- DataTables -->
        <div class="card mb-3">
          <div class="card-header">
            <a href="#" class='fas fa-user'>Tambah Pendaftar</a>
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover" id="dataTables" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>Kode Registrasi</th>
                    <th>Nama Lengkap</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Telepon</th>
                    <th>Aksi</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $no=1; if( ! empty($regist)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
                  foreach ($regist as $data){ ?>
                  <tr>
                    <td>
                      <?php echo $no++; ?>
                    </td>
                    <td width="150">
                      <?php echo $data->kode_regist ?>
                    </td>
                    <td>
                      <?php echo $data->nama ?>
                    </td>
                    <td>
                      <?php echo $data->username?>
                    </td>
                    <td>
                      <?php echo $data->password?>
                    </td>
                    <td>
                      <?php echo $data->telepon?>
                    </td>
                    <td>
                      <a href="<?php echo site_url('admin/kriteria/ubah/'.$data->kode_regist) ?>"
                       class="btn btn-small">
                       <i class="fas fa-edit"></i> Edit</a>
                      <a onclick="deleteConfirm('<?php echo site_url('admin/master_data/hapus/'.$data->kode_regist) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
                    </td>
                  </tr>
                  <?php } //end foreach
                  } else{ // Jika data siswa kosong
                        echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
                  } ?>

                </tbody>

              </table>
              
            </div>
          </div>
        </div>
      </div>

      <?php $this->load->view("admin/_partials/footer") ?>
      
    </div>

  </div>
  <?php $this->load->view("admin/_partials/scrolltop") ?>
  <?php $this->load->view("admin/_partials/modal") ?>

  <?php $this->load->view("admin/_partials/js") ?>
  
  <script type="text/javascript">
    $(document).ready(function(){
      $('#dataTables').DataTable();
    });
  </script>

  <script type="text/javascript">
    function deleteConfirm(url){
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

</body>
</html>