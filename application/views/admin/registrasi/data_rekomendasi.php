<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('admin/_partials/head')?>
</head>

<body id='page-top'>
  <?php $this->load->view('admin/_partials/navbar')?>

  <div id="wrapper">
    <?php $this->load->view('admin/_partials/sidebar')?>
    <div id="content-wrapper">
      <div class="container-fluid">
        <?php $this->load->view('admin/_partials/breadcrumbs')?>

        <!-- DataTables -->
        <div class="card mb-3" style="border-color: black">
          <div class="card-header" style="text-align: center; background-color: skyblue">
            Data Nilai Awal
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataAwal" width="100%" cellspacing="0">
                <thead style="text-align: center">
                  <tr>
                    <th>NO</th>
                    <th>Nama Lengkap</th>
                    <th>Nilai B.Inggris (C1)</th>
                    <th>Nilai B. Indonesia (C2)</th>
                    <th>Nilai Matematika (C3)</th>
                    <th>Rata-rata Nilai UN (C4)</th>                    
                    <th>Penghasilan Orang Tua (C5)</th>
                  </tr>
                </thead>

                <tbody style="text-align: center;">
                  <?php $no=1; if( ! empty($nilai)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
                  foreach ($nilai as $a){ ?>
                  <tr>
                    <td>
                      <?php echo $no++; ?>
                    </td>
                    <td>
                      <?php echo $a->nama ?>
                    </td>
                    <td>
                      <?php echo $a->C1 ?>
                    </td>
                    <td>
                      <?php echo $a->C2 ?>
                    </td>
                    <td>
                      <?php echo $a->C3 ?>
                    </td>
                    <td>
                      <?php echo $a->C4 ?>
                    </td>
                    <td>
                      <?php echo $a->C5 ?>
                    </td>
                    
                  </tr>
                  <?php } //end foreach
                  } else{ // Jika data siswa kosong
                        echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
                  } ?>

                </tbody>
                <tfoot style="text-align: center">
                  <tr>
                    <td style="background-color: lightgrey" colspan="2">
                      <b>PEMBAGI</b>
                    </td>
                      <?php 
                        $pembagi_C1 = 0;
                        $pembagi_C2 = 0;
                        $pembagi_C3 = 0;
                        $pembagi_C4 = 0;
                        $pembagi_C5 = 0;
                      
                      foreach ($nilai as $a){
                        @$pembagi_C1 = $pembagi_C1 + pow($a->C1,2);
                        @$pembagi_C2 = $pembagi_C2 + pow($a->C2,2);
                        @$pembagi_C3 = $pembagi_C3 + pow($a->C3,2);
                        @$pembagi_C4 = $pembagi_C4 + pow($a->C4,2);
                        @$pembagi_C5 = $pembagi_C5 + pow($a->C5,2);

                      }?>
                      <td style="background-color: lightgrey"><b><?= round(sqrt($pembagi_C1),3)?></b></td>
                      <td style="background-color: lightgrey"><b><?= round(sqrt($pembagi_C2),3)?></b></td>
                      <td style="background-color: lightgrey"><b><?= round(sqrt($pembagi_C3),3)?></b></td>
                      <td style="background-color: lightgrey"><b><?= round(sqrt($pembagi_C4),3)?></b></td>
                      <td style="background-color: lightgrey"><b><?= round(sqrt($pembagi_C5),3)?></b></td>
                  </tr>
                </tfoot>
              </table>
              
            </div>
          </div>
        </div>
        <br/>
        <div class="card mb-3" style="border-color: black">
          <div class="card-header" style="text-align: center; background-color: skyblue">
            Data Matriks Ternormalisasi
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataNormalisasi" width="100%" cellspacing="0">
                <thead style="text-align: center">
                  <tr>
                    <th>NO</th>
                    <th>Nama Lengkap</th>
                    <th>Nilai B.Inggris (C1)</th>
                    <th>Nilai B. Indonesia (C2)</th>
                    <th>Nilai Matematika (C3)</th>
                    <th>Rata-rata Nilai UN (C4)</th>                    
                    <th>Penghasilan Orang Tua (C5)</th>
                  </tr>
                </thead>

                <tbody style="text-align: center;">
                  <?php $no=1; if( ! empty($nilai)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
                  foreach ($nilai as $a){ ?>
                  <tr>
                    <td>
                      <?php echo $no++; ?>
                    </td>
                    <td>
                      <?php echo $a->nama ?>
                    </td>
                    <td>
                      <?php @$ter_C1=round($a->C1/sqrt($pembagi_C1),3); echo $ter_C1 ?>
                    </td>
                    <td>
                      <?php @$ter_C2=round($a->C2/sqrt($pembagi_C2),3); echo $ter_C2 ?>
                    </td>
                    <td>
                      <?php @$ter_C3=round($a->C3/sqrt($pembagi_C3),3); echo $ter_C3 ?>
                    </td>
                    <td>
                      <?php @$ter_C4=round($a->C4/sqrt($pembagi_C4),3); echo $ter_C4 ?>
                    </td>
                    <td>
                      <?php @$ter_C5=round($a->C5/sqrt($pembagi_C5),3); echo $ter_C5 ?>
                    </td>
                    
                  </tr>
                  <?php } //end foreach
                  } else{ // Jika data siswa kosong
                        echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
                  } ?>

                </tbody>
              </table>
              
            </div>
          </div>
        </div>
        <br/>
        <div class="card mb-3" style="border-color: black">
          <div class="card-header" style="text-align: center; background-color: skyblue">
            Data Matriks Normalisasi Terbobot
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataMatriksTerbobot" width="100%" cellspacing="0">
                <thead style="text-align: center">
                  <tr>
                    <th>NO</th>
                    <th>Nama Lengkap</th>
                    <th>Nilai B.Inggris (C1)</th>
                    <th>Nilai B. Indonesia (C2)</th>
                    <th>Nilai Matematika (C3)</th>
                    <th>Rata-rata Nilai UN (C4)</th>                    
                    <th>Penghasilan Orang Tua (C5)</th>
                  </tr>
                </thead>

                <tbody style="text-align: center;">
                  <?php $no=1; $i=0; if( ! empty($nilai)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada

                  foreach ($bobot as $b=>$bob) {
                    $bobot_kriteria[]=(
                      $bob->bobot
                    );
                  }
                  foreach ($nilai as $a){ ?>
                  <tr>
                    <td>
                      <?php echo $no++; ?>
                    </td>
                    <td>
                      <?php echo $a->nama ?>
                    </td>
                    <td>
                      <?php echo @$array_C1[]=(round(($a->C1/sqrt($pembagi_C1))*$bobot_kriteria[0],3))?>
                    </td>
                    <td>
                      <?php echo @$array_C2[]=(round(($a->C2/sqrt($pembagi_C2))*$bobot_kriteria[1],3)) ?>
                    </td>
                    <td>
                      <?php echo @$array_C3[]=(round(($a->C3/sqrt($pembagi_C3))*$bobot_kriteria[2],3)) ?>
                    </td>
                    <td>
                      <?php echo @$array_C4[]=(round(($a->C4/sqrt($pembagi_C4))*$bobot_kriteria[3],3)) ?>
                    </td>
                    <td>
                      <?php echo @$array_C5[]=(round(($a->C5/sqrt($pembagi_C5))*$bobot_kriteria[4],3)) ?>
                    </td>
                    
                  </tr>
                  <?php }//end foreach
                  } else{ // Jika data siswa kosong
                        echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
                  }?>

                </tbody>
              </table>
              
            </div>
          </div>
        </div>
        <br/>
        <div class="card mb-3" style="border-color: black">
          <div class="card-header" style="text-align: center; background-color: skyblue">
            <i>Positive Ideal Solution</i> & <i> Negative Ideal Solution</i>
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" width="100%" cellspacing="0">
                <thead style="text-align: center">
                  <tr>
                    <th>PIS / NIS</th>
                    <th>Nilai B.Inggris (C1)</th>
                    <th>Nilai B. Indonesia (C2)</th>
                    <th>Nilai Matematika (C3)</th>
                    <th>Rata-rata Nilai UN (C4)</th>                    
                    <th>Penghasilan Orang Tua (C5)</th>
                  </tr>
                </thead>

                <tbody style="text-align: center;">
                  <?php $no=1; $i=0; if( ! empty($nilai)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada

                  foreach ($bobot as $b=>$bob) {
                    $bobot_kriteria[]=(
                      $bob->bobot
                    );
                  }?>
                  <tr>
                    <td>
                      PIS
                    </td>
                    <td><?= @$PIS_C1=MAX($array_C1)?></td>
                    <td><?= @$PIS_C2=MAX($array_C2)?></td>
                    <td><?= @$PIS_C3=MAX($array_C3)?></td>
                    <td><?= @$PIS_C4=MAX($array_C4)?></td>
                    <td><?= @$PIS_C5=MAX($array_C5)?></td>
                  </tr>
                  <tr>
                    <td>
                      NIS
                    </td>
                    <td><?= @$NIS_C1=MIN($array_C1)?></td>
                    <td><?= @$NIS_C2=MIN($array_C2)?></td>
                    <td><?= @$NIS_C3=MIN($array_C3)?></td>
                    <td><?= @$NIS_C4=MIN($array_C4)?></td>
                    <td><?= @$NIS_C5=MIN($array_C5)?></td>
                  </tr>
                  <!-- end foreach -->
                  <?php } else{ // Jika data siswa kosong
                        echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
                  }?>

                </tbody>
              </table>
              
            </div>
          </div>
        </div>
        <br/>
        <div class="card mb-3" style="border-color: black">
          <div class="card-header" style="text-align: center; background-color: skyblue">
            Positive Ideal Solution
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataPis" width="100%" cellspacing="0">
                <thead style="text-align: center">
                  <tr>
                    <th colspan="6"><i>Positive Ideal Solution</i></th>             
                    <th style="background-color: lightgrey">D+</th>
                  </tr>
                </thead>

                <tbody style="text-align: center">
                  <?php $no=1; $i=0; if( ! empty($nilai)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
                  foreach ($nilai as $a){ ?>
                    <tr>
                      <td>
                        <?php echo $no++; ?>
                      </td>
                      <td>
                        <?php echo @$D_A1=(round(pow(($PIS_C1-$array_C1[$i]),2),3))?>
                      </td>
                      <td>
                        <?php echo @$D_A2=(round(pow(($PIS_C2-$array_C2[$i]),2),3)) ?>
                      </td>
                      <td>
                        <?php echo @$D_A3=(round(pow(($PIS_C3-$array_C3[$i]),2),3)) ?>
                      </td>
                      <td>
                        <?php echo @$D_A4=(round(pow(($PIS_C4-$array_C4[$i]),2),3)) ?>
                      </td>
                      <td>
                        <?php echo @$D_A5=(round(pow(($PIS_C5-$array_C5[$i]),2),3)) ?>
                      </td>
                      <td>
                        <?= @$hasil_PIS[]=(round(SQRT($D_A1+$D_A2+$D_A3+$D_A4+$D_A5),3))?>
                      </td>
                      
                    </tr>
                  <?php $i++;}//end foreach
                  } else{ // Jika data siswa kosong
                        echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
                  }?>

                </tbody>
              </table>
              
            </div>
          </div>
        </div>
        <br/>
        <div class="card mb-3" style="border-color: black">
          <div class="card-header" style="text-align: center; background-color: skyblue">
            Negative Ideal Solution
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataNis" width="100%" cellspacing="0">
                <thead style="text-align: center">
                  <tr>
                    <th colspan="6"><i>Negative Ideal Solution</i></th>             
                    <th style="background-color: lightgrey">D-</th>
                  </tr>
                </thead>

                <tbody style="text-align: center;">
                  <?php $no=1; $i=0; foreach ($nilai as $a): ?>
                    <tr>
                      <td>
                        <?php echo $no++; ?>
                      </td>
                      <td>
                        <?php echo @$D_A1=(round(pow(($NIS_C1-$array_C1[$i]),2),3))?>
                      </td>
                      <td>
                        <?php echo @$D_A2=(round(pow(($NIS_C2-$array_C2[$i]),2),3)) ?>
                      </td>
                      <td>
                        <?php echo @$D_A3=(round(pow(($NIS_C3-$array_C3[$i]),2),3)) ?>
                      </td>
                      <td>
                        <?php echo @$D_A4=(round(pow(($NIS_C4-$array_C4[$i]),2),3)) ?>
                      </td>
                      <td>
                        <?php echo @$D_A5=(round(pow(($NIS_C5-$array_C5[$i]),2),3)) ?>
                      </td>
                      <td>
                        <?= @$hasil_NIS[]=(round(SQRT($D_A1+$D_A2+$D_A3+$D_A4+$D_A5),3))?>
                      </td>
                      
                    </tr>
                  <?php $i++; endforeach?>

                </tbody>
              </table>
              
            </div>
          </div>
        </div>
        <br/>
        <div class="card mb-3" style="border-color: black">
          <div class="card-header" style="text-align: center; background-color: skyblue">
            Tabel Perangkingan
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <?php 
              if ($a->id_jurusan == 1){
                @$jurus = 'hasil_tkr';
              }else if ($a->id_jurusan == 2){
                @$jurus = 'hasil_atph';
              }else if ($a->id_jurusan == 3){
                @$jurus = 'hasil_tsm';
              }else if ($a->id_jurusan == 4){
                @$jurus = 'hasil_rpl';
              }else{
                @$jurus = 'hasil_aphp';
              }
              $this->db->truncate($jurus);
                $i=0; foreach ($nilai as $a){
                  $hasil = $hasil_NIS[$i]/($hasil_NIS[$i]+$hasil_PIS[$i]);
                $data=array(
                    'id_regist'=>$a->id_regist,
                    'nilai'=>round($hasil, 3),
                );

                $this->M_master_data->hasil_akhir($data, $jurus);
                $i++;
              }?>
              <table class="table table-bordered" id="dataTables" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><i>Ranking</i></th>
                    <th>Nama</th>
                    <th>Nilai</th>
                  </tr>
                </thead>
                <?php $data= $this->M_master_data->ranking($jurus); ?>
                <tbody>                  
                  <?php $no=1; foreach($data as $rank) :?>
                    <tr>
                        <td width="150">
                          <?php echo $no++ ?>
                        </td>
                        <td>
                          <?php echo $rank->nama ?>
                        </td>
                        <td>
                          <?php echo $rank->nilai ?>
                        </td>
                      </tr>
                   <?php endforeach?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <?php $this->load->view("admin/_partials/footer") ?>
      
    </div>

  </div>
  <?php $this->load->view("admin/_partials/scrolltop") ?>
  <?php $this->load->view("admin/_partials/modal") ?>

  <?php $this->load->view("admin/_partials/js") ?>
  
  <script type="text/javascript">
    $(document).ready(function(){
      $('#dataAwal').DataTable();
      $('#dataNormalisasi').DataTable();
      $('#dataMatriksTerbobot').DataTable();
      $('#dataTables').DataTable();
      $('#dataPis').DataTable();
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#dataNis').DataTable();
    });
  </script>

  <script type="text/javascript">
    function deleteConfirm(url){
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

</body>
</html>