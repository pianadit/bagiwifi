<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('admin/_partials/head');?>
</head>
<body id='page-top'>
	<?php $this->load->view('admin/_partials/navbar');?>

	<div id="wrapper">
		<?php $this->load->view('admin/_partials/sidebar');?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<?php $this->load->view('admin/_partials/breadcrumbs');?>

				<?php if ($this->session->flashdata('success')):?>
				<div class="alert alert-success" role='alert'>
					<?php echo $this->session->flashdata('success');?>
				</div>
			<?php endif ?>

				<!-- Konten -->
				<div class="card mb-3">
					<div class="card-header">
						<h3 align="center"> Edit Data Siswa</h3>
					</div>
					<div class="card-body">
						<?php foreach($datasiswa as $a=>$datasiswa){?>

						<form action="<?php echo site_url("admin/siswa/aksi_edit/".$datasiswa->kode_regist)?>" method="post">
							
							<div class="form-group">

								<label for="name">NISN*</label>
								<input type="text" class="form-control" name="nik" value="<?php echo set_value('nisn', $datasiswa->nisn); ?>" readonly>
								
							</div>

							<div class="form-group">
								<label>Nama Lengkap*</label>
								<input type="text" class="form-control" name="nama" value="<?php echo set_value('nama', $datasiswa->nama); ?>">								
							</div>

							<div class="form-group">
								<label>Jenis Kelamin*</label>
								<input class="form-control" type="text" name="jk" value="<?php echo set_value('jk', $datasiswa->jk)?>" />
								
							</div>
							<div class="form-group">
								<label>Username*</label>
								<input class="form-control" type="text" name="username" value="<?php echo set_value('username', $datasiswa->username)?>" />
								
							</div>
							<div class="form-group">
								<label>Password*</label>
								<input class="form-control" type="text" name="password" value="<?php echo set_value('password', $datasiswa->password)?>" />
								
							</div>
							<div class="form-group">
								<label>No Telepon*</label>
								<input class="form-control" type="text" name="telepon" value="<?php echo set_value('telepon', $datasiswa->telepon)?>" />
								
							</div>
							<div class="form-group">
								<label>Tempat Lahir*</label>
								<input class="form-control" type="text" name="tmpt_lahir" value="<?php echo set_value('tmpt_lahir', $datasiswa->tmpt_lahir)?>" />
								
							</div>
							<div class="form-group">
								<label>Tanggal Lahir*</label>
								<input class="form-control" type="date" name="tgl_lahir" value="<?php echo set_value('tgl_lahir', $datasiswa->tgl_lahir)?>" />
								
							</div>
							
							<div class="form-group">
								<label>Nama Ayah*</label>
								<input class="form-control" type="text" name="nama_ayah" value="<?php echo set_value('nama_ayah', $datasiswa->nama_ayah)?>" />
								
							</div>
							<div class="form-group">
								<label>Nama Ibu*</label>
								<input class="form-control" type="text" name="nama_ibu" value="<?php echo set_value('nama_ibu', $datasiswa->nama_ibu)?>" />
								
							</div>
							<div class="form-group">
								<label>Pendidikan Ayah*</label>
								<input class="form-control" type="text" name="pendidikan_ayah" value="<?php echo set_value('pendidikan_ayah', $datasiswa->pendidikan_ayah)?>" />
								
							</div>
							<div class="form-group">
								<label>Pendidikan Ibu*</label>
								<input class="form-control" type="text" name="pendidikan_ibu" value="<?php echo set_value('pendidikan_ibu', $datasiswa->pendidikan_ibu)?>" />
								
							</div>
							<input type="submit" name="submit" value="Ubah">
							<a href="<?php echo site_url('admin/home/data_siswa'); ?>"><input type="button" value="Batal"></a>
						<?php }?>

							
						</form>

					</div>
					<div class="card-footer small text-muted">
						* required fields
					</div>
				</div>
			</div>

			<?php $this->load->view("admin/_partials/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("admin/_partials/scrolltop") ?>
	<?php $this->load->view("admin/_partials/modal") ?>

	<?php $this->load->view("admin/_partials/js") ?>

</body>
</html>