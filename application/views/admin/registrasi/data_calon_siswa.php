<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('admin/_partials/head')?>
</head>

<body id='page-top'>
  <?php $this->load->view('admin/_partials/navbar')?>

  <div id="wrapper">
    <?php $this->load->view('admin/_partials/sidebar')?>
    <div id="content-wrapper">
      <div class="container-fluid">
        <?php $this->load->view('admin/_partials/breadcrumbs')?>

        <!-- DataTables -->
        <div class="card mb-3">
          <div class="card-header" style="text-align: center">
            Data Calon Siswa
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover" id="dataTables" width="100%" cellspacing="0">
                <thead style="text-align: center">
                  <tr>
                    <th>NO</th>
                    <th>Nama Lengkap</th>
                    <th>Nilai B.Inggris (C1)</th>
                    <th>Nilai B. Indonesia (C2)</th>
                    <th>Nilai Matematika (C3)</th>
                    <th>Rata-rata Nilai UN (C4)</th>                    
                    <th>Penghasilan Orang Tua (C5)</th>
                  </tr>
                </thead>

                <tbody style="text-align: center;">
                  <?php $no=1; if( ! empty($calon_siswa)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
                  foreach ($calon_siswa as $a){ ?>
                  <tr>
                    <td>
                      <?php echo $no++; ?>
                    </td>
                    <td>
                      <?php echo $a->nama ?>
                    </td>
                    <td>
                      <?php echo $a->C1 ?>
                    </td>
                    <td>
                      <?php echo $a->C2 ?>
                    </td>
                    <td>
                      <?php echo $a->C3 ?>
                    </td>
                    <td>
                      <?php echo $a->C4 ?>
                    </td>
                    <td>
                      <?php echo $a->C5 ?>
                    </td>
                    
                  </tr>
                  <?php } //end foreach
                  } else{ // Jika data siswa kosong
                        echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
                  } ?>

                </tbody>

              </table>
              
            </div>
          </div>
        </div>
      </div>

      <?php $this->load->view("admin/_partials/footer") ?>
      
    </div>

  </div>
  <?php $this->load->view("admin/_partials/scrolltop") ?>
  <?php $this->load->view("admin/_partials/modal") ?>

  <?php $this->load->view("admin/_partials/js") ?>
  
  <script type="text/javascript">
    $(document).ready(function(){
      $('#dataTables').DataTable();
    });
  </script>

  <script type="text/javascript">
    function deleteConfirm(url){
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

</body>
</html>