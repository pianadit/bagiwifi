<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('admin/_partials/head');?>
</head>
<body id='page-top'>
	<?php $this->load->view('admin/_partials/navbar');?>

	<div id="wrapper">
		<?php $this->load->view('admin/_partials/sidebar');?>
		<div id="content-wrapper">
			<div class="container-fluid">
				
				<?php if ($this->session->flashdata('success')):?>
				<div class="alert alert-success" role='alert'>
					<?php echo $this->session->flashdata('success');?>
				</div>
			<?php endif ?>

				<!-- Konten -->
				<div class="card mb-3">
					
					<div class="card-body">
						
						<table class="table table-striped table-responsive table-bordered">
 
								<tr>
									<td colspan="3" align="center"><H4>TANDA BUKTI PENGAJUAN PENDAFTARAN PPDB<br</H4></td>
								</TR>
								
								<tr class="info">
									<td colspan="3" align="center"><h4>DATA SISWA</h4></td>
								</tr>
								<?php foreach($datauser as $data) {?>
								
								<tr>
									<th style="width:20%;">Nomor Induk Siswa Nasional(NISN)</th>
									
									<td><?php echo $data->nisn;?></td>
								</tr>
								<tr>
									<th>Nama</th>
									
									<td><?php echo $data->nama;?></td>
								</tr>
								<tr>
									<th>Jenis Kelamin</th>
									
									<td><?php echo $data->jk;?></td>
								</tr>
								<tr>
									<th>No Telepon/HP</th>
									
									<td><?php echo $data->telepon;?></td>
								</tr>
								
								<tr>
									<th>Tempat Tanggal Lahir</th>
									
									<td><?php echo $data->tmpt_lahir.", ".$data->tgl_lahir;?></td>
								</tr>
								<tr>
									<th>Username</th>
									
									<td><?php echo $data->username;?></td>
								</tr>
								
								<tr>
									<th>Password</th>
									
									<td><?php echo $data->password;?></td>
								</tr>
								<tr>
									<th>Agama</th>
									
									<td><?php echo $data->agama;?></td>
								</tr>
								<tr>
									<th>Jurusan</th>
									
									<td><?php echo $data->jurusan;?></td>
								</tr>

								<tr class="info">
									<td colspan="3" align="center"><h4>DATA NILAI</h4></td>
								</tr>
								<tr>
									<th>B. Inggris</th>
									
									<td><?php echo $data->C1;?></td>
								</tr>
								<tr>
									<th>B. Indonesia</th>
									
									<td><?php echo $data->C2;?></td>
								</tr>
								<tr>
									<th>Matematika</th>
									
									<td><?php echo $data->C3;?></td>
								</tr>
								<tr>
									<th>Rata-rata Nilai UN</th>
									
									<td><?php echo $data->C4;?></td>
								</tr>
								<tr>
									<th>Penghasilan Orang Tua</th>
									
									<td><?php echo $data->pendapatan;?></td>
								</tr>

																
								<tr class="info">
									<td colspan="3" align="center"><h4>DATA ORANG TUA</h4></td>
								</tr>
								<tr>
									<th>Nama Ayah</th>
									
									<td><?php echo $data->nama_ayah;?></td>
								</tr>
								<tr>
									<th>Nama Ibu</th>
									
									<td><?php echo $data->nama_ibu;?></td>
								</tr>
								<tr>
									<th>Pendidikan Tertinggi Ayah</th>
									
									<td><?php echo $data->pendidikan_ayah;?></td>
								</tr>
								<tr>
									<th>Pendidikan Tertinggi Ibu</th>
									
									<td><?php echo $data->pendidikan_ibu;?></td>
								</tr>
								
							<?php } ?>
							 
							</table>

					</div>
					<div class="card-footer small text-muted">
						* required fields
					</div>
				</div>
			</div>

			<?php $this->load->view("admin/_partials/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("admin/_partials/scrolltop") ?>
	<?php $this->load->view("admin/_partials/modal") ?>

	<?php $this->load->view("admin/_partials/js") ?>

</body>
</html>