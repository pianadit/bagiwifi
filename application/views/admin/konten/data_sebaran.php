<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('admin/_partials/head')?>
</head>

<body id='page-top'>
  <?php $this->load->view('admin/_partials/navbar')?>

  <div id="wrapper">
    <?php $this->load->view('admin/_partials/sidebar')?>
    <div id="content-wrapper">
      <div class="container-fluid">
        <?php $this->load->view('admin/_partials/breadcrumbs')?>

        <!-- DataTables -->
        <div class="card mb-3">
          <div class="card-header">
            <a href="<?php echo site_url('admin/home/add_sebar')?>">Tambah Sebaran</a>
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover" id="dataTables" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>Nama Wifi</th>
                    <th>Lokasi</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $no=1;foreach ($admin as $data): ?>
                  <tr>
                    <td>
                      <?php echo $no++; ?>
                    </td>
                    <td>
                      <?php echo $data->nama_wifi ?>
                    </td>
                    <td>
                      <?php echo $data->lokasi?>
                    </td>
                    <td>
                      <?php echo $data->status ?>
                    </td>
                    <td>
                      <a href="<?php echo site_url('admin/home/edit/'.$data->kode_sebar) ?>"
                       class="btn btn-small">
                       <i class="fas fa-edit"></i> Edit</a>
                      <a onclick="deleteConfirm('<?php echo site_url('admin/home/hapus/'.$data->kode_sebar) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>

              </table>
              
            </div>
          </div>
        </div>
      </div>

      <?php $this->load->view("admin/_partials/footer") ?>
      
    </div>

  </div>
  <?php $this->load->view("admin/_partials/scrolltop") ?>
  <?php $this->load->view("admin/_partials/modal") ?>

  <?php $this->load->view("admin/_partials/js") ?>
  
  <script type="text/javascript">
    $(document).ready(function(){
      $('#dataTables').DataTable();
    });
  </script>

  <script type="text/javascript">
    function deleteConfirm(url){
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

</body>
</html>