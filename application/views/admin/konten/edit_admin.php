<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('admin/_partials/head');?>
</head>
<body id='page-top'>
	<?php $this->load->view('admin/_partials/navbar');?>

	<div id="wrapper">
		<?php $this->load->view('admin/_partials/sidebar');?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<?php $this->load->view('admin/_partials/breadcrumbs');?>

				<?php if ($this->session->flashdata('success')):?>
				<div class="alert alert-success" role='alert'>
					<?php echo $this->session->flashdata('success');?>
				</div>
			<?php endif ?>

				<!-- Konten -->
				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/products')?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="<?php echo site_url("admin/home/edit/".$admin->kode_sebar)?>" method="post">
							<div class="form-group">
								<label for="name">Kode Sebar</label>
								<input type="text" class="form-control" name="kode" value="<?php echo set_value('kode', $admin->kode_sebar); ?>" readonly>
								
							</div>

							<div class="form-group">
								<label>Nama Wifi</label>
								<input type="text" class="form-control" name="nama" value="<?php echo set_value('nama', $admin->nama_wifi); ?>">								
							</div>

							<div class="form-group">
								<label>Lokasi</label>
								<input class="form-control" type="text" name="lokasi" value="<?php echo set_value('lokasi', $admin->lokasi)?>" />
								
							</div>
							<div class="form-group">
								<label>Status</label>
								<input class="form-control" type="text" name="status" value="<?php echo set_value('status', $admin->status)?>" />
								
							</div>
							<div class="form-group">
								<label>Password*</label>
								<input class="form-control" type="text" name="password" value="<?php echo set_value('password', $admin->password)?>" />
								
							</div>
							<input type="submit" name="submit" value="Ubah">
							<a href="<?php echo base_url('#'); ?>"><input type="button" value="Batal"></a>

							
						</form>

					</div>
					<div class="card-footer small text-muted">
						* required fields
					</div>
				</div>
			</div>

			<?php $this->load->view("admin/_partials/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("admin/_partials/scrolltop") ?>
	<?php $this->load->view("admin/_partials/modal") ?>

	<?php $this->load->view("admin/_partials/js") ?>

</body>
</html>