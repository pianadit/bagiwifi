<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('admin/_partials/head')?>
</head>

<body id='page-top'>
  <?php $this->load->view('admin/_partials/navbar')?>

  <div id="wrapper">
    <?php $this->load->view('admin/_partials/sidebar')?>
    <div id="content-wrapper">
      <div class="container-fluid">
        <?php $this->load->view('admin/_partials/breadcrumbs')?>

        <!-- DataTables -->
        <div class="card mb-3">
          <div class="card-header">
            <a href="<?php echo site_url('admin/kriteria/tambah')?>">Tambah Kriteria</a>
          </div>

	        <div class="card-body">

				<div class="jumbotron">
				  <h1>Selamat Datang <?php echo $this->session->userdata('nama');?>! </h1>
				  <p>Anda adalah admin di aplikasi Sebar WiFi</p>
				  <hr>
				  <h3><p><div class="alert alert-success" role="alert">Akun anda <?php echo $this->session->userdata('status');?>!</div> </p></h3>						  
				</div>
				
			</div>
          </div>
        </div>
      </div>

      <?php $this->load->view("admin/_partials/footer") ?>
      
    </div>

  </div>
  <?php $this->load->view("admin/_partials/scrolltop") ?>
  <?php $this->load->view("admin/_partials/modal") ?>

  <?php $this->load->view("admin/_partials/js") ?>
  
  <script type="text/javascript">
    $(document).ready(function(){
      $('#dataTables').DataTable();
    });
  </script>

  <script type="text/javascript">
    function deleteConfirm(url){
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

</body>
</html>