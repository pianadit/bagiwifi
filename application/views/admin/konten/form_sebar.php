<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('admin/_partials/head');?>
</head>
<body id='page-top'>
	<?php $this->load->view('admin/_partials/navbar');?>

	<div id="wrapper">
		<?php $this->load->view('admin/_partials/sidebar');?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<?php $this->load->view('admin/_partials/breadcrumbs');?>

				<?php if ($this->session->flashdata('success')):?>
				<div class="alert alert-success" role='alert'>
					<?php echo $this->session->flashdata('success');?>
				</div>
			<?php endif ?>

				<!-- Konten -->
				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/home/data_sebar')?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="<?php echo site_url('admin/home/tambah_sebar')?>" id="formInput" method="post">
							<?php $info=$this->session->flashdata('error');
								if (!empty($info))
								{
									echo $info;
								}
							?>
							<div class="form-group">
								<label for="name">Kode Sebar</label>
								<input class="form-control type="text" name="kode" required='text'  />
								
							</div>

							<div class="form-group">
								<label>Nama Wifi</label>
								<input class="form-control" type="text" name="nama" required='text' placeholder="Nama Wifi" />
								
							</div>

							<div class="form-group">
								<label>Lokasi</label>
								<input class="form-control" type="text" name="lokasi" />
								
							</div>

							<div class="form-group">
								<label>Status</label>
								<input class="form-control" type="text" name="status"/>
								
							</div>
												
							<input class="btn btn-success" type="submit" id="save" name="btn" value="Saved" />
						</form>

					</div>
					<!-- <div class="card-footer small text-muted">
						* required fields
					</div> -->
				</div>
			</div>

			<?php $this->load->view("admin/_partials/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("admin/_partials/scrolltop") ?>
	<?php $this->load->view("admin/_partials/modal") ?>

	<?php $this->load->view("admin/_partials/js") ?>

</body>
</html>