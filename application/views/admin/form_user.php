<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('admin/_partials/head');?>
</head>
<body id='page-top'>
	<?php $this->load->view('admin/_partials/navbar');?>

	<div id="wrapper">
		<?php $this->load->view('admin/_partials/sidebar');?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<?php $this->load->view('admin/_partials/breadcrumbs');?>

				<?php if ($this->session->flashdata('success')):?>
				<div class="alert alert-success" role='alert'>
					<?php echo $this->session->flashdata('success');?>
				</div>
			<?php endif ?>

				<!-- Konten -->
				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/products')?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="<?php echo site_url('admin/Master_data/add')?>" id="formInput" method="post">
							<div class="form-group">
								<label for="name">NIK/NIP*</label>
								<input class="form-control type="text" name="nik" required='number' placeholder="No Induk Karyawan/Pendidik" />
								
							</div>

							<div class="form-group">
								<label>Nama*</label>
								<input class="form-control" type="text" name="nama" required='text' placeholder="Nama Lengkap" />
								
							</div>

							<div class="form-group">
								<label>Email*</label>
								<input class="form-control" type="Email" name="email" required='Email' placeholder="Email" />
								
							</div>

							<div class="form-group">
								<label>Username*</label>
								<input class="form-control" type="text" name="username" required='text' placeholder="Username" />
								
							</div>
							<div class="form-group">
								<label>Password*</label>
								<input class="form-control" type="Password" name="password" required='password' placeholder="Password" />
								
							</div>
							<div class="form-group">
                                    <label>Keterangan</label>
                                    <select name="ket" class="form-control" required="">
                                            <option value="" disabled="">Keterangan</option>
                                                <option value="admin">Admin</option>
                                                <option value="siswa">Siswa</option>
                                        </select>                                
                                </div>
							
							<input class="btn btn-success" type="submit" id="save" name="btn" value="Saved" />
						</form>

					</div>
					<div class="card-footer small text-muted">
						* required fields
					</div>
				</div>
			</div>

			<?php $this->load->view("admin/_partials/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("admin/_partials/scrolltop") ?>
	<?php $this->load->view("admin/_partials/modal") ?>

	<?php $this->load->view("admin/_partials/js") ?>

</body>
</html>