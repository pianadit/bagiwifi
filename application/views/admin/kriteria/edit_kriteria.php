<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('admin/_partials/head');?>
</head>
<body id='page-top'>
	<?php $this->load->view('admin/_partials/navbar');?>

	<div id="wrapper">
		<?php $this->load->view('admin/_partials/sidebar');?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<?php $this->load->view('admin/_partials/breadcrumbs');?>

				<?php if ($this->session->flashdata('success')):?>
				<div class="alert alert-success" role='alert'>
					<?php echo $this->session->flashdata('success');?>
				</div>
			<?php endif ?>

				<!-- Konten -->
				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/products')?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="<?php echo site_url("admin/kriteria/ubah/".$kriteria->id_kriteria)?>" method="post">
							<div class="form-group">
								<label for="name">ID Kriteria*</label>
								<input type="text" class="form-control" name="id_kriteria" value="<?php echo set_value('id_kriteria', $kriteria->id_kriteria); ?>" readonly>
								
							</div>

							<div class="form-group">
								<label>Nama Kriteria*</label>
								<input type="text" class="form-control" name="nama_kriteria" value="<?php echo set_value('nama_kriteria', $kriteria->nama_kriteria); ?>">								
							</div>

							<div class="form-group">
								<label>Bobot*</label>
								<input class="form-control" type="text" name="bobot" value="<?php echo set_value('nilai', $kriteria->bobot)?>" />
								
							</div>
							<input type="submit" name="submit" value="Ubah">
							<a href="<?php echo base_url('admin/kriteria'); ?>"><input type="button" value="Batal"></a>

							
						</form>

					</div>
					<div class="card-footer small text-muted">
						* required fields
					</div>
				</div>
			</div>

			<?php $this->load->view("admin/_partials/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("admin/_partials/scrolltop") ?>
	<?php $this->load->view("admin/_partials/modal") ?>

	<?php $this->load->view("admin/_partials/js") ?>

</body>
</html>