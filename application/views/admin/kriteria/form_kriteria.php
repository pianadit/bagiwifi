<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('admin/_partials/head');?>
</head>
<body id='page-top'>
	<?php $this->load->view('admin/_partials/navbar');?>

	<div id="wrapper">
		<?php $this->load->view('admin/_partials/sidebar');?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<?php $this->load->view('admin/_partials/breadcrumbs');?>

				<?php if ($this->session->flashdata('success')):?>
				<div class="alert alert-success" role='alert'>
					<?php echo $this->session->flashdata('success');?>
				</div>
			<?php endif ?>

				<!-- Konten -->
				<div class="card mb-3">
					
					<div class="card-body">

						<form action="<?php echo site_url('admin/kriteria/tambah')?>" method="post">
							<div class="form-group">
								<label for="name">ID Kriteria</label>
								<input class="form-control type="text" name="id_kriteria" required='numeric' placeholder="ID Kriteria" />
								
							</div>

							<div class="form-group">
								<label>Nama Kriteria*</label>
								<input class="form-control" type="text" name="nama_kriteria" required='text' placeholder="Nama Kriteria" />
								
							</div>

							<div class="form-group">
								<label>Nilai Kriteria*</label>
								<input class="form-control" type="text" name="nilai" required='text' placeholder="Nilai Kriteria" />								
							</div>

							<input type="submit" name="submit" value="Simpan">
							<a href="<?php echo site_url('admin/kriteria'); ?>"><input type="button" value="Batal"></a>

							
						</form>

					</div>
					<div class="card-footer small text-muted">
						* required fields
					</div>
				</div>
			</div>

			<?php $this->load->view("admin/_partials/footer") ?>
			
		</div>

	</div>
	<?php $this->load->view("admin/_partials/scrolltop") ?>
	<?php $this->load->view("admin/_partials/modal") ?>

	<?php $this->load->view("admin/_partials/js") ?>

</body>
</html>