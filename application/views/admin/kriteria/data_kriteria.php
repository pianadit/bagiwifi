<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('admin/_partials/head')?>
</head>

<body id='page-top'>
  <?php $this->load->view('admin/_partials/navbar')?>

  <div id="wrapper">
    <?php $this->load->view('admin/_partials/sidebar')?>
    <div id="content-wrapper">  
        <div class="container col-md-12">
          <?php $this->load->view('admin/_partials/breadcrumbs')?>

          <!-- DataTables -->
            <div class="card col-md-6">
              <div class="card-header">
                Tabel Kriteria
              </div>
                <div class="table-striped">
                  <table class="table table-hover" id="dataTables" cellspacing="0">
                    <thead>
                      <tr>
                        <th>NO</th>
                        <th>ID Kriteria</th>
                        <th>Nama Kriteria</th>
                        <th>Bobot</th>
                        <th>Action</th>
                      </tr>
                    </thead>

                    <tbody>
                      <?php $no=1;foreach ($kriteria as $a): ?>
                      <tr>
                        <td>
                          <?php echo $no++; ?>
                        </td>
                        <td width="150">
                          <?php echo $a->id_kriteria ?>
                        </td>
                        <td>
                          <?php echo $a->nama_kriteria ?>
                        </td>
                        <td>
                          <?php echo $a->bobot?>
                        </td>
                        <td>
                          <a href="<?php echo site_url('admin/kriteria/ubah/'.$a->id_kriteria) ?>"
                           class="btn btn-small">
                           <i class="fas fa-edit"></i> Edit</a>
                        </td>
                      </tr>
                      <?php endforeach; ?>

                    </tbody>

                  </table>
                  
                </div>
            </div>
            <br/>
            <div class="card col-md-6">
              <div class="card-header">
                Tabel Pendapatan
              </div>
                <div class="table-striped">
                  <table class="table table-hover" id="dataTables">
                    <thead>
                      <tr>
                        <th>NO</th>
                        <th>Pendapatan</th>
                        <th>Nilai</th>
                      </tr>
                    </thead>
                      <?php $no=1;foreach ($pendapatan as $a): ?>
                      <tr>
                        <td>
                          <?php echo $no++; ?>
                        </td>
                        <td>
                          <?php echo $a->pendapatan?>
                        </td>
                        <td>
                          <?php echo $a->nilai ?>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                    <tbody>
                    </tbody>

                  </table>
                  
                </div>
            </div>
            <br/>
        </div>
      <?php $this->load->view("admin/_partials/footer") ?>
      
    </div>

  </div>
  <?php $this->load->view("admin/_partials/scrolltop") ?>
  <?php $this->load->view("admin/_partials/modal") ?>

  <?php $this->load->view("admin/_partials/js") ?>
  
  <!-- <script type="text/javascript">
    $(document).ready(function(){
      $('#dataTables').DataTable();
    });
  </script> -->

  <script type="text/javascript">
    function deleteConfirm(url){
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    }
  </script>

</body>
</html>