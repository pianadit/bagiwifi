<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
  public function __construct(){
    parent::__construct();
  $this->load->model('Model_login');
}
 public function index()
 {
  $this->load->view('view_login');
 }

 function proses_login()
 {
    $username = $this->input->post('username',true);
    $password = $this->input->post('password',true);
    
    $akun2 = $this->Model_login->login_admin($username,$password);
    $akun = count($akun2);
    
    // if ($akun > 0)
    // {
      $data_session = array(
                  'nik'=>$akun2->nik,
                  'nama'=>$akun2->nama,
                  'username'=>$akun2->username,
                  'logged_in'=>true
      );
      
      $this->session->set_userdata($data_session);
      redirect('admin');
    // }
    // else
    // {
    //   $akun2 = $this->Model_login->login_siswa($username,$password);
    //   $akun = count($akun2);

    //   if ($akun > 0)
    //   {
    //     $data_session = array(
    //                 'kode_regist'=>$akun2->kode_regist,
    //                 'nama_jurusan'=>$akun2->jurusan,
    //                 'jurusan'=>$akun2->id_jurusan,
    //                 'nama'=>$akun2->nama,
    //                 'username'=>$akun2->username,
    //                 'telepon'=>$akun2->telepon,
    //                 'logged_in'=>true
    //     );
        
    //     $this->session->set_userdata($data_session);
    //    redirect('user/home');
    //   }
    //   else{
        
    //   redirect('login');
    //   }
    //   //$this->template->ppdb('ppdb/content');
    // }
  }

  function logout()
  {
    $this->session->sess_destroy();
    redirect('welcome');
  }

}