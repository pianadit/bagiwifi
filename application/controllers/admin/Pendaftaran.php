<?php
class Pendaftaran extends CI_Controller{
	
	function __construct(){
        parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('login');
        }
        $this->load->model('Pendaftaran_model');
        
    }

    public function index(){
    	$data['agama'] = $this->Pendaftaran_model->get_agama();

    	$data['jk'] = $this->Pendaftaran_model->get_jenis_kelamin();
    	$this->load->view('admin/product/form_pendaftaran', $data);
    }

}