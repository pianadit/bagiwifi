<?php

class Siswa extends CI_Controller {
    public function __construct()
    {
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('login');
		}
		$this->load->model('Madmin');
		$this->load->model('M_master_data');
	}

	public function index()
	{
		$data['datasiswa'] = $this->M_master_data->ambil_data_siswa($kode_regist);
		$this->load->view('admin/registrasi/data_siswa', $data); 
	}

	public function tampil_edit($kode_regist)
	{	
	    $data['datasiswa'] = $this->M_master_data->ambil_data_siswa($kode_regist);
	    $this->load->view('admin/registrasi/edit_data_siswa', $data); 
	}

	public function aksi_edit($kode_regist)
	{	
            $nama = $this->input->post('nama');
            $jenis_kelamin = $this->input->post('jk');
            $tempat = $this->input->post('tmpt_lahir');
            $tgl_lahir = $this->input->post('tgl_lahir');
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $telepon = $this->input->post('telepon');
            $nama_ayah = $this->input->post('nama_ayah');
            $nama_ibu = $this->input->post('nama_ibu');
            $pendidikan_ayah = $this->input->post('pendidikan_ayah');
            $pendidikan_ibu = $this->input->post('pendidikan_ibu');

           
                    $data_siswa = array(   
                            
                            
                            'jk' => $jenis_kelamin,
                            'tmpt_lahir' => $tempat,
                            'tgl_lahir' => $tgl_lahir,
                                                        
                            'nama_ayah' => $nama_ayah,
                            'nama_ibu' => $nama_ibu,
                            'pendidikan_ayah' => $pendidikan_ayah,
                            'pendidikan_ibu' => $pendidikan_ibu
                        );
                    $data_regist = array(  
                            
                            'nama' => $nama,
                            
                            'username' => $username,
                            'password' => $password,
                            
                            'telepon' => $telepon,
                            
                        );
                    $this->M_master_data->aksi_edit($data_siswa,$kode_regist);
                    $this->M_master_data->aksi_edit_regist($data_regist,$kode_regist);                    
                    redirect('admin/home/data_siswa');
                 
	}

	public function hapus($kode_regist){
    $this->M_master_data->hapus_regist($kode_regist); // Panggil fungsi delete() yang ada di Kriteria_model.php
    $this->M_master_data->hapus_siswa($kode_regist);
    redirect('admin/home/data_siswa');
  }

  public function detail_siswa($kode_regist){
    	
		$data['datauser'] = $this->M_master_data->ambil_data_siswa($kode_regist);
		$this->load->view('admin/registrasi/detail_data', $data);
    }
}