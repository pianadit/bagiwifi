<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kriteria extends CI_Controller {
  
  public function __construct(){
    parent::__construct();
    if(!$this->session->userdata('logged_in')){
      redirect('login');
    }
    
    $this->load->model('Kriteria_model'); // Load Kriteria_model ke controller ini
  }
  
  public function index(){
    $data['kriteria'] = $this->Kriteria_model->view();
    $data['pendapatan'] = $this->Kriteria_model->pendapatan();
    $this->load->view('admin/kriteria/data_kriteria', $data);
  }
  
  public function tambah(){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      // if($this->Kriteria_model->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->Kriteria_model->save(); // Panggil fungsi save() yang ada di Kriteria_model.php
        redirect('admin/kriteria');
      
    }
    
    $this->load->view('admin/kriteria/form_kriteria');
  }
  
  public function ubah($id_kriteria){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      // if($this->Kriteria_model->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->Kriteria_model->edit($id_kriteria); // Panggil fungsi edit() yang ada di Kriteria_model.php
        redirect('admin/kriteria');
      // }
    }
    
    $data['kriteria'] = $this->Kriteria_model->view_by($id_kriteria);
    $this->load->view('admin/kriteria/edit_kriteria', $data);
  }
  
  public function hapus($id_kriteria){
    $this->Kriteria_model->delete($id_kriteria); // Panggil fungsi delete() yang ada di Kriteria_model.php
    redirect('admin/kriteria');
  }
}