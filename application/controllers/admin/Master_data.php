<?php defined('BASEPATH') OR exit ('No direct access allowed');

class Master_data extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('login');
		}
		$this->load->model('M_master_data');
	}
	public function index()
	{
		$this->load->view('admin/form_user');
	}

	public function edit_data_siswa()
	{    
		$data['datasiswa'] = $this->M_master_data->ambil_data();
		$this->load->view('admin/registrasi/data_siswa', $data);
    }

	public function tampil_registrasi(){
		$data['regist'] = $this->M_master_data->view();
    	$this->load->view('admin/registrasi/data_registrasi', $data);
	}

	public function data_calon_siswa(){
		$data['calon_siswa'] = $this->M_master_data->data_calon_siswa();
    	$this->load->view('admin/registrasi/data_calon_siswa', $data);
	}

	public function hasil_rekomendasi($id_jurusan){
		$data['nilai'] = $this->M_master_data->data_calon_siswa($id_jurusan);
		$data['bobot'] = $this->M_master_data->bobot();
		$data['jml_kri'] = $this->M_master_data->jml_kri();
		$data['jml_alt'] = $this->M_master_data->jml_alt();
    	$this->load->view('admin/registrasi/data_rekomendasi', $data);
	}

	public function hapus($kode_regist){
		$this->M_master_data->delete($kode_regist); // Panggil fungsi delete() yang ada di m_master_data.php
    	redirect('admin/Master_data/tampil_registrasi');
	}

	public function product()
	{
		$this->load->view('admin/product');
	}

	public function add()
	{
		$nik = $this->input->post('nik');
    	$nama = $this->input->post('nama');
    	$email = $this->input->post('email');
    	$username = $this->input->post('username');
    	$password = $this->input->post('password');
    	$ket = $this->input->post('ket');
       
       $data = array(	'id' => $nik,
						'nama' => $nama,
						'email' => $email,
						'username' => $username,
						'password' => $password,
						'level' => $ket
					);
        	$res=$this->M_master_data->save($data,'users');
        	echo json_encode($res);        
	}
}