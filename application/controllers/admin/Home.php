<?php

class Home extends CI_Controller {
    public function __construct()
    {
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('login');
		}
		$this->load->model('Madmin');
	}

	public function index()
	{
		$data['admin'] = $this->Madmin->tampilAdmin();
        $this->load->view('admin/konten/utama',$data); 
	}


	public function tampil_registrasi(){
		$data['regist'] = $this->M_master_data->get_registrasi();
    	$this->load->view('admin/registrasi/data_registrasi', $data);
	}

	public function data_sebaran()
	{
		$this->load->model('Msebaran', 'msebaran');
		$data['admin'] = $this->msebaran->tampilData();
        $this->load->view('admin/konten/data_sebaran',$data); 
	}

	public function add_sebar()
	{
        $this->load->view('admin/konten/form_sebar'); 
	}

	public function tambah_sebar()
	{
		$this->load->model('Msebaran');
        $kode = $this->input->post('kode');
    	$nama = $this->input->post('nama');
    	$lokasi = $this->input->post('lokasi');
    	$status = $this->input->post('status');

    	$cek_kode = $this->Msebaran->cek_kode($kode);
        

            if ($cek_kode) //kondisi jika status sudah ada
                {
                    $this->session->set_flashdata('error','<div class="alert alert-warning" role="alert">Maaf Kode sudah ada yang menggunakan</div>');
                    redirect('admin/home/add_sebar');
                }
                else
                {
                    $data = array(	
                    	'kode_sebar' => $kode,
						'nama_wifi' => $nama,
						'lokasi' => $lokasi,
						'status' => $status,
					);
			        $this->Msebaran->simpan_data($data);                    
			        redirect('admin/home/data_sebaran');
                }        	
	}

	public function data_siswa()
	{    
		$data['datasiswa'] = $this->Madmin->ambil_data();
		$this->load->view('admin/registrasi/data_siswa', $data);
    }

	public function edit($nik){
	    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
	      // if($this->Madmin->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
	        $this->Madmin->edit_admin($nik); // Panggil fungsi edit() yang ada di Madmin.php
	        redirect('admin/home/data_admin');
	      // }
	    }
	    $this->load->model('Msebaran');
	    $data['admin'] = $this->Msebaran->tampilData($nik);
	    $this->load->view('admin/konten/edit_admin', $data);
	}

  	public function hapus($nik){
	    $this->Madmin->hapus_admin($nik); // Panggil fungsi delete() yang ada di Kriteria_model.php
	    redirect('admin/home/data_admin');
	}
}