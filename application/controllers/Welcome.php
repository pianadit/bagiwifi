<?php
class Welcome extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('UserModel');
    }
 
    public function index(){
        $this->load->view('umum/konten/home_umum');
    }

    public function prosedur(){
        $this->load->view('umum/konten/prosedur');
    }

    public function tampil_regist(){
        $data['kode'] = $this->UserModel->buat_kode(); //kode otomatis tanpa auto increment
        $data['tampil'] = $this->UserModel->tampil();
        $this->load->view('umum/konten/form_registrasi', $data);
    }
    

    public function registrasi(){
        if($_POST){
        $kode_registrasi = $this->input->post('kode_registrasi');
        $nama = $this->input->post('nama');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $telepon = $this->input->post('telepon');

        $cek_username = $this->UserModel->cek_username($username);
        $num_account = count($cek_username);

        if ($num_account > 0) //kondisi jika username sudah ada
            {
                $this->session->set_flashdata('error','<div class="alert alert-warning" role="alert">Maaf username sudah ada yang menggunakan</div>');
                redirect('welcome/tampil_regist');
            }
            else
            {
                $this->UserModel->save_regist(array(  'kode_regist' => $kode_registrasi,
                        'nama' => $nama,
                        'username' => $username,
                        'password' => $password,
                        'telepon' => $telepon
                     ));
                $this->session->set_flashdata('error','<div class="alert alert-success" role="alert">Pendaftaran berhasil. Silahkan Login dengan menggunakan Akun yang sudah didaftarkan</div>');
                redirect('welcome');
            }

        }
    }
}