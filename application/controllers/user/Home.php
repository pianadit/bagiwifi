<?php

class Home extends CI_Controller {
    public function __construct()
    {
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('welcome');
		}
		$this->load->model('UserModel');
      $this->load->model('M_master_data');
      $this->load->model('Model_login');
	}

	public function index()	
	{        
        $data['registrasi'] = $this->UserModel->tampil();
        $this->load->view('user/konten/utama');        
	}	

	//menampilkan form untuk melengkapi biodata
	public function tampil_biodata($kode_regist='')
	{
		$kode_regist=$_SESSION['kode_regist'];
        $cek_biodata = $this->UserModel->cek_data($kode_regist);
        // $biodata = count($cek_biodata);
        
        if ($cek_biodata>0)
        {
			redirect('user/home/preview_data');
		}
		else{
			$kode_regist = $this->session->userdata('kode_regist');
			$data['registrasi'] = $this->UserModel->select_kode_regist($kode_regist);
			$data['pendapatan'] = $this->UserModel->select_nilai();
			$data['jurusan'] = $this->UserModel->select_jurusan();
			$this->load->view('user/konten/form_biodata', $data);
		}
				
    }
    public function preview_data(){
    	
		$data['datauser'] = $this->UserModel->ambil_data();
		$this->load->view('user/konten/preview_data', $data);
    }

    public function data_ranking(){
    	$this->load->view('user/konten/hasil_rekomendasi');
    }
}