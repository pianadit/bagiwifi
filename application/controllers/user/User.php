<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
   {
      parent::__construct();
      if(!$this->session->userdata('logged_in')){
            redirect('welcome');
        }
      $this->load->model('UserModel');
           
   }

	public function index()
	{  
        $logged_in = $this->session->userdata('logged_in');
        
        if (!empty($logged_in))
        {
            $data['registrasi'] = $this->UserModel->tampil();
            $this->load->view('user/konten/utama');
        }
        else
        {
            redirect('base_url()');
        }
        
	}
        
   
    public function data_user()
    {
        $this->load->view('user/konten/data_user');
    }

    public function ubah($id_kriteria){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->Kriteria_model->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->Kriteria_model->edit($id_kriteria); // Panggil fungsi edit() yang ada di Kriteria_model.php
        redirect('admin/kriteria');
      }
    }
    
    $data['kriteria'] = $this->Kriteria_model->view_by($id_kriteria);
    $this->load->view('admin/kriteria/edit_kriteria', $data);
  }


    public function lengkapi_biodata(){
        
            $kode_regist = $this->input->post('kode_regist');
            $nisn = $this->input->post('nisn');
            $nama = $this->input->post('nama');
            $jenis_kelamin = $this->input->post('jenis_kelamin');
            $tempat = $this->input->post('tempat');
            $tgl_lahir = $this->input->post('tgl_lahir');
            $agama = $this->input->post('agama');
            $jurusan = $this->input->post('jurusan');
            $inggris = $this->input->post('inggris');
            $indonesia = $this->input->post('indonesia');
            $matematika = $this->input->post('matematika');
            $rata = $this->input->post('rata-rata');
            $pendapatan = $this->input->post('pendapatan');
            $nama_ayah = $this->input->post('nama_ayah');
            $nama_ibu = $this->input->post('nama_ibu');
            $pendidikan_ayah = $this->input->post('pendidikan_ayah');
            $pendidikan_ibu = $this->input->post('pendidikan_ibu');

            $num_account = $this->UserModel->cek_nisn($nisn);
            // $num_account = count($cek_nisn);

            if ($num_account > 0) //kondisi jika username sudah ada
                {
                    $this->session->set_flashdata('error','<div class="alert alert-warning" role="alert">Maaf NISN sudah ada yang menggunakan</div>');
                    redirect('user/home/tampil_biodata');
                }
                else
                {
                    $data = array(   'kode_regist' => $kode_regist,
                            'nisn' => $nisn,
                            'jk' => $jenis_kelamin,
                            'tmpt_lahir' => $tempat,
                            'agama' => $agama,
                            'jurusan' => $jurusan,
                            'tgl_lahir' => $tgl_lahir,
                            'nama_ayah' => $nama_ayah,
                            'nama_ibu' => $nama_ibu,
                            'pendidikan_ayah' => $pendidikan_ayah,
                            'pendidikan_ibu' => $pendidikan_ibu
                        );
                    $data_nilai = array(
                           'id_regist' => $kode_regist,
                            'id_jurusan' => $jurusan,
                            'C1' => $inggris,
                            'C2' => $indonesia,
                            'C3' => $matematika,
                            'C4' => $rata,
                            'C5' => $pendapatan,
                    );
                    $this->UserModel->lengkapi_biodata($data);
                    $this->UserModel->data_nilai($data_nilai);                    
                    redirect('user/user');
                }
                 
    }

        

    public function dataadmin()
	{ 
            
        if ($this->session->userdata('login') == TRUE){
            $data['admin']= $this->madmin->tampilkan();  
            $isi =  $this->template->display('admin/content/vdataadmin',$data);
            $this->load->view('admin/vutama',$isi);
            } else { redirect('clogin'); }
	}
        
       
        
        public function tambahadmin()
	{
            
        if ($this->session->userdata('login') == TRUE){
            $isi =  $this->template->display('admin/content/vtambahadmin');
            $this->load->view('admin/vutama',$isi);
            } else { redirect('clogin'); }
	}

        public function editadmin()
	{
            
        if ($this->session->userdata('login') == TRUE){
            $data['admin'] = $this->madmin->get_by_id($this->uri->segment(3));
            $isi =  $this->template->display('admin/content/vtambahadmin',$data);
            $this->load->view('admin/vutama',$isi);
            } else { redirect('clogin'); }
	}    
}