<?php
class UserModel extends CI_Model{
 
    public function tampil()   {    
      return $this->db->get('registrasi')->result();
    }

    function select_kode_regist($kode_regist)
    {
      $this->db->select('*');
      $this->db->from('registrasi');
      $this->db->where('kode_regist',$kode_regist);
      
      return $this->db->get()->row();
    }

    public function select_nilai(){
      return $this->db->get('pendapatan')->result();
    }
    public function select_jurusan(){
      return $this->db->get('jurusan')->result();
    }

    public function ambil_data(){
      $this->db->select('*');
      $this->db->from('tbl_siswa');
      $this->db->join('registrasi','tbl_siswa.kode_regist = registrasi.kode_regist');
      $this->db->join('data_nilai','registrasi.kode_regist = data_nilai.id_regist');
      $this->db->join('pendapatan','data_nilai.C5 = pendapatan.nilai');
      $this->db->join('jurusan','tbl_siswa.jurusan = jurusan.id_jurusan');
      $this->db->where('tbl_siswa.kode_regist',$_SESSION['kode_regist']);
      
      $query = $this->db->get();
      return $query->result();
    }

    function cek_username($username)
    {
      $this->db->select(COUNT('*'));
      $this->db->from('registrasi');
      $this->db->where('username',$username);
      
      return $this->db->get()->row();
    }
    function cek_data($kode_regist) //mengecek data di tbl_siswa dengan parameter kode_regist
    {
      $this->db->select('*');
      $this->db->from('tbl_siswa');
      $this->db->where('kode_regist',$kode_regist);
      
      return $this->db->get()->row();
    }

    function cek_nisn($nisn)
    {
      $this->db->select(COUNT('*'));
      $this->db->from('tbl_siswa');
      $this->db->where('nisn',$nisn);
      
      return $this->db->get()->row();
    }
    
    public function save_regist($data){
      $this->db->insert('registrasi',$data);
    }

         //kode otomatis tanpa auto increment
  public function buat_kode(){
      $this->db->select('RIGHT(registrasi.kode_regist, 3) as kode', FALSE);
      $this->db->order_by('kode_regist','DESC');
      $this->db->limit(1);

      $query = $this->db->get('registrasi');

      if ($query->num_rows()<>0) {
          $data = $query->row();
          $kode = intval($data->kode)+1;
      }else{
        $kode = 1;
      }
      $kode_max = str_pad($kode,3,"0",STR_PAD_LEFT);
      $kode_jadi = "PSB".$kode_max;
      return $kode_jadi;
  }

    public function lengkapi_biodata($data){
      $this->db->insert('tbl_siswa',$data);
    }

    public function data_nilai($data){
      $this->db->insert('data_nilai',$data);
    }
 
}