<?php 
class Msebaran extends CI_Model
{
    
    public function tampilData()
    {
       $query = $this->db->get('sebaran_wifi');
        return $query->result();
    }

 //    public function get_nik($nik){
	//     $this->db->where('nik', $nik);
	//     return $this->db->get('login')->row();
 //  	}

    public function simpan_data($data){
      	$this->db->insert('sebaran_wifi',$data);
    }

 //    public function edit_admin($nik){
	//     $data = array(
	//       "nik" => $this->input->post('nik'),
	//       "nama" => $this->input->post('nama'),
	//       "email" => $this->input->post('email'),
	//       "username" => $this->input->post('username'),
	//       "password" => $this->input->post('password')      
	//     );
	    
	//     $this->db->where('nik', $nik);
	//     $this->db->update('login', $data); // Untuk mengeksekusi perintah update data
	// }

	// public function hapus_admin($nik){
	//    $this->db->where('nik', $nik);
	//    $query = $this->db->delete('login'); // Untuk mengeksekusi perintah delete data
	//    return$query;
	// }

    public function cek_kode($kode)
    {
      $this->db->select('*');
      $this->db->from('sebaran_wifi');
      $this->db->where('kode_sebar',$kode);
      
      return $this->db->get()->row();
    }

 //    public function ambil_data(){
 //    	$this->db->select('*');
 //      	$this->db->from('tbl_siswa');
 //     	$this->db->join('registrasi','tbl_siswa.kode_regist = registrasi.kode_regist');
      
 //      $query = $this->db->get();
 //      return $query->result();
 //    }

	// public function get_jenis_kelamin(){
	// 	$this->db->select('*')
	// 			->from('jenis_kelamin');
	// 	$hasil = $this->db->get();
	// 	return $hasil->result();
	// }
	// public function get_agama(){
	// 	$this->db->select('*')
	// 			->from('agama');
	// 	$hasil = $this->db->get();
	// 	return $hasil->result();
	// }

	// public function get_registrasi(){
 //    	return $this->db->get('registrasi')->result();
 //  	}
}