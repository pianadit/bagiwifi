<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kriteria_model extends CI_Model {
  // Fungsi untuk menampilkan semua data kriteria
  public function view(){
    return $this->db->get('kriteria')->result();
  }

  public function pendapatan(){
    return $this->db->get('pendapatan')->result();
  }
  
  // Fungsi untuk menampilkan data kriteria berdasarkan NIS nya
  public function view_by($id_kriteria){
    $this->db->where('id_kriteria', $id_kriteria);
    return $this->db->get('kriteria')->row();
  }
  
  // Fungsi untuk validasi form tambah dan ubah
  public function validation($mode){
    $this->load->library('form_validation'); // Load library form_validation untuk proses validasinya
    
    // Tambahkan if apakah $mode save atau update
    // Karena ketika update, NIS tidak harus divalidasi
    // Jadi NIS di validasi hanya ketika menambah data kriteria saja
    if($mode == "save")
      $this->form_validation->set_rules('id_kriteria', 'ID Kriteria', 'required|numeric|max_length[11]');
    
    $this->form_validation->set_rules('nama_kriteria', 'Nama Kriteria', 'required|max_length[50]');
    $this->form_validation->set_rules('nilai', 'Nilai', 'required');
      
    if($this->form_validation->run()){ // Jika validasi benar
      return TRUE; // Maka kembalikan hasilnya dengan TRUE
    }else{ // Jika ada data yang tidak sesuai validasi
      return FALSE; // Maka kembalikan hasilnya dengan FALSE
    }
  }
  
  // Fungsi untuk melakukan simpan data ke tabel kriteria
  public function save(){
    $data = array(
      "id_kriteria" => $this->input->post('id_kriteria'),
      "nama_kriteria" => $this->input->post('nama_kriteria'),
      "nilai" => $this->input->post('nilai')
    );
    
    $this->db->insert('kriteria', $data); // Untuk mengeksekusi perintah insert data
  }
  
  // Fungsi untuk melakukan ubah data kriteria berdasarkan NIS kriteria
  public function edit($id_kriteria){
    $data = array(
      "id_kriteria" => $this->input->post('id_kriteria'),
      "nama_kriteria" => $this->input->post('nama_kriteria'),
      "bobot" => $this->input->post('bobot')
    );
    
    $this->db->where('id_kriteria', $id_kriteria);
    $this->db->update('kriteria', $data); // Untuk mengeksekusi perintah update data
  }
  
  // Fungsi untuk melakukan menghapus data kriteria berdasarkan NIS kriteria
  public function delete($id_kriteria){
    $this->db->where('id_kriteria', $id_kriteria);
   $query = $this->db->delete('kriteria'); // Untuk mengeksekusi perintah delete data
   return$query;
  }
}