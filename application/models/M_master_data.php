<?php defined('BASEPATH') or exit('No direct script access allowed');

class M_master_data extends CI_Model
{
	
	public function view(){
    	return $this->db->get('registrasi')->result();
  	}

 //  	public function validation($mode){
	//     $this->load->library('form_validation'); // Load library form_validation untuk proses validasinya
	    
	//     // Tambahkan if apakah $mode save atau update
	//     // Karena ketika update, NIS tidak harus divalidasi
	//     // Jadi NIS di validasi hanya ketika menambah data kriteria saja
	//     if($mode == "save")
	//       $this->form_validation->set_rules('id_kriteria', 'ID Kriteria', 'required|numeric|max_length[11]');
	    
	//     $this->form_validation->set_rules('nama_kriteria', 'Nama Kriteria', 'required|max_length[50]');
	//     $this->form_validation->set_rules('nilai', 'Nilai', 'required');
	      
	//     if($this->form_validation->run()){ // Jika validasi benar
	//       return TRUE; // Maka kembalikan hasilnya dengan TRUE
	//     }else{ // Jika ada data yang tidak sesuai validasi
	//       return FALSE; // Maka kembalikan hasilnya dengan FALSE
	//     }
	// }

	public function save($data,$table)
	{

		$res=$this->db->insert($table,$data);
		return $res;
	}

  	public function delete($kode_regist){
  		$this->db->where('kode_regist', $kode_regist);
	   	$query = $this->db->delete('registrasi'); // Untuk mengeksekusi perintah delete data
	   	return$query;
  	}
	
	public function ambil_data_siswa($kode_regist){
    	$this->db->select('*');
      $this->db->from('tbl_siswa');
     	$this->db->join('registrasi','tbl_siswa.kode_regist = registrasi.kode_regist');
      $this->db->join('data_nilai','registrasi.kode_regist = data_nilai.id_regist');
      $this->db->join('pendapatan','data_nilai.C5 = pendapatan.nilai');
     	$this->db->where('tbl_siswa.kode_regist', $kode_regist);      
      $query = $this->db->get();
      return $query->result();
    }

    public function data_calon_siswa($id_jurusan){
    	$this->db->select('*');
      	$this->db->from('data_nilai');
     	$this->db->join('registrasi','data_nilai.id_regist = registrasi.kode_regist');
      $this->db->where('id_jurusan',$id_jurusan);     
      $query = $this->db->get();
      return $query->result();
    }

    public function bobot(){
    	$this->db->select('bobot');
    	$this->db->from('kriteria');
    	$query = $this->db->get();
      	return $query->result();
    }

    public function data_nilai(){
    	$this->db->select('id_jurusan, C1, C2, C3, C4, C5');
      	$this->db->from('data_nilai');    
		$query = $this->db->get();
	    return $query->result();
    }

    public function jml_kri(){
    	$this->db->select('id_kriteria');
    	$this->db->from('kriteria');
    	$query = $this->db->get();
      	return $query->num_rows();
    }

    public function jml_alt(){
    	$this->db->select('id');
    	$this->db->from('data_nilai');
    	$query = $this->db->get();
      	return $query->num_rows();
    }

    public function hasil_akhir($data, $jurus){
		$this->db->insert($jurus, $data);
	}

	public function ranking($jurus){
  	$this->db->select('*');
  	$this->db->from($jurus);
   	$this->db->join('registrasi', $jurus.'.id_regist = registrasi.kode_regist');  
   	$this->db->order_by('nilai', 'DESC');
  	$query = $this->db->get();
    	return $query->result();
  }

  public function rank_siswa($jurus){
    $this->db->select('*');
    $this->db->from($jurus);
    $this->db->join('registrasi', $jurus.'.id_regist = registrasi.kode_regist');  
    $this->db->order_by('nilai', 'DESC');
    $query = $this->db->get();
      return $query->result();
  }

    public function aksi_edit($data_siswa, $kode_regist){
  		
    $this->db->where('kode_regist', $kode_regist);
    $this->db->update('tbl_siswa', $data_siswa);
  	}

    public function aksi_edit_regist($data_regist, $kode_regist){
	    $this->db->where('kode_regist', $kode_regist);
	    $this->db->update('registrasi', $data_regist);
  	}

  	public function hapus_regist($kode_regist){
	   $this->db->where('kode_regist', $kode_regist);
	   $query = $this->db->delete('registrasi'); // Untuk mengeksekusi perintah delete data
	   return$query;
	}

  	public function hapus_siswa($kode_regist){
	   $this->db->where('kode_regist', $kode_regist);
	   $query = $this->db->delete('tbl_siswa'); // Untuk mengeksekusi perintah delete data
	   return$query;
	}
  		
}