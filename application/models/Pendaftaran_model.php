<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pendaftaran_model extends CI_Model{

	function get_jenis_kelamin(){
		$this->db->select('*')
				->from('jenis_kelamin');
		$hasil = $this->db->get();
		return $hasil->result();
	}
	function get_agama(){
		$this->db->select('*')
				->from('agama');
		$hasil = $this->db->get();
		return $hasil->result();
	}
}