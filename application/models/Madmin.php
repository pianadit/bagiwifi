<?php 
class Madmin extends CI_Model
{
    
    public function tampilAdmin()
    {
       $query = $this->db->get('login');
        return $query->result();
    }

    public function get_nik($nik){
	    $this->db->where('nik', $nik);
	    return $this->db->get('login')->row();
  	}

    public function simpan_admin($data){
      	$this->db->insert('login',$data);
    }

    public function edit_admin($nik){
	    $data = array(
	      "nik" => $this->input->post('nik'),
	      "nama" => $this->input->post('nama'),
	      "email" => $this->input->post('email'),
	      "username" => $this->input->post('username'),
	      "password" => $this->input->post('password')      
	    );
	    
	    $this->db->where('nik', $nik);
	    $this->db->update('login', $data); // Untuk mengeksekusi perintah update data
	}

	public function hapus_admin($nik){
	   $this->db->where('nik', $nik);
	   $query = $this->db->delete('login'); // Untuk mengeksekusi perintah delete data
	   return$query;
	}

    public function cek_nik($nik)
    {
      $this->db->select('*');
      $this->db->from('login');
      $this->db->where('nik',$nik);
      
      return $this->db->get()->row();
    }

    public function ambil_data(){
    	$this->db->select('*');
      	$this->db->from('tbl_siswa');
     	$this->db->join('registrasi','tbl_siswa.kode_regist = registrasi.kode_regist');
      
      $query = $this->db->get();
      return $query->result();
    }

	public function get_jenis_kelamin(){
		$this->db->select('*')
				->from('jenis_kelamin');
		$hasil = $this->db->get();
		return $hasil->result();
	}
	public function get_agama(){
		$this->db->select('*')
				->from('agama');
		$hasil = $this->db->get();
		return $hasil->result();
	}

	public function get_registrasi(){
    	return $this->db->get('registrasi')->result();
  	}
}