<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_login extends CI_Model {
	
 function login_siswa($username,$password)
	{
		$this->db->select('*');
		$this->db->from('registrasi');
		// $this->db->join('tbl_siswa', 'registrasi.kode_regist= tbl_siswa.kode_regist');
		// $this->db->join('jurusan', 'tbl_siswa.jurusan= jurusan.id_jurusan');
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		
		return $this->db->get()->row();
	}

	public function lihat_hasil()
	{
		$this->db->select('*');
		$this->db->from('tbl_siswa');
		$this->db->join('jurusan', 'tbl_siswa.jurusan= jurusan.id_jurusan');
		$this->db->where('kode_regist', $_SESSION['kode_regist']);
		
		return $this->db->get()->row();
	}

	function login_admin($username,$password)
	{
		$this->db->select('*');
		$this->db->from('login');
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		
		return $this->db->get()->row();
	}
}