<?php defined('BASEPATH') or exit('No direct script access allowed');

class Product_model extends CI_Model
{
	private $_table = 'products';

	public $product_id;
	public $name;
	public $price;
	public $image ='default.jpg';
	public $description;

	
	public function getAll()
	{
		return $this->db->get($this->_table)->result();
	}

	public function getById($id)
	{
		$this->db->SELECT('*');
		$this->db->from('products');
		$this->db->where('product_id',$id);
		$qury = $this->db->get();
		
		return $qury->result();
	}

	public function save()
	{
		$post = $this->input->post();
		$this->product_id = uniqid();
		$this->name = $post['name'];
		$this->price = $post['price'];
		$this->description = $post['description'];

		$this->db->insert($this->_table,$this);
	}
	
	//coba edit
	//function edit_data($where,$table){		
	//return $this->db->get_where($_table,$where);
	//}
	//function update_data($where,$data,$table){
	//	$this->db->where($where);
	//	$this->db->update($_table,$data);
	//}

	
	//function update(){
	// $name = $this->input->post('name');
	// $price = $this->input->post('price');
	// $description = $this->input->post('description');
 
	// $data = array(
		// 'name' => $name,
		// 'price' => $price,
		// 'description' => $description
	// );
 
	// $where = array(
		//'id' => $id
	// );
 
	// $this->product_model->update_data($where,$data,'user');
	// redirect('admin/products');
	// }

	public function update()
	{
		$post = $this->input->post();
		$this->product_id = $post['id'];
		$this->name = $post['name'];
		$this->price = $post['price'];
		$this->description = $post['description'];

		$this->db->update($this->_table, $this, array('product_id' => $post['id']));
	}

	public function delete($where,$table){
		$this->db->where($where);
	$this->db->delete($table);
	}
}