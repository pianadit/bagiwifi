/*
SQLyog Ultimate v13.1.1 (32 bit)
MySQL - 10.1.38-MariaDB : Database - db_bagiwifi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_bagiwifi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_bagiwifi`;

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `id_admin` varchar(50) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `login` */

insert  into `login`(`id_admin`,`nama`,`username`,`password`) values 
('00001','administrator','admin','admin');

/*Table structure for table `mlokasi` */

DROP TABLE IF EXISTS `mlokasi`;

CREATE TABLE `mlokasi` (
  `id_lokasi` varchar(50) NOT NULL,
  `nama_lokasi` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_lokasi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mlokasi` */

/*Table structure for table `mwifi` */

DROP TABLE IF EXISTS `mwifi`;

CREATE TABLE `mwifi` (
  `id_wifi` varchar(50) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_wifi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mwifi` */

/*Table structure for table `sebaran_wifi` */

DROP TABLE IF EXISTS `sebaran_wifi`;

CREATE TABLE `sebaran_wifi` (
  `kode_sebar` varchar(50) NOT NULL,
  `nama_wifi` varchar(150) DEFAULT NULL,
  `lokasi` varchar(150) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode_sebar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sebaran_wifi` */

insert  into `sebaran_wifi`(`kode_sebar`,`nama_wifi`,`lokasi`,`status`) values 
('002','tes 2','krw brt','pending'),
('SW001','tes','krw','approve');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
